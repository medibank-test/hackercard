<?php

namespace app\controllers;


use Yii;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;

class ArticleController extends Controller
{

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\HttpCache',
                'only' => ['index'],
                'lastModified' => function ($action, $params) {
                    return time();
                },
                'sessionCacheLimiter' => 'public',
                'cacheControlHeader' => 'public, max-age=36',
            ],
        ];
    }
    /**
     * Yii action controller
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
//        Yii::$app->response->format = Response::FORMAT_JSON;

        $response = [];
        $urls = [];
        $chs = [];

        //set the urls
        $urls[] = Yii::$app->params['articleUrl'];
        $urls[] = Yii::$app->params['imageUrl'] . Yii::$app->params['accessKey'];

        //create the array of cURL handles and add to a multi_curl
        $mh = curl_multi_init();
        foreach ($urls as $key => $url) {
            $chs[$key] = curl_init($url);
            curl_setopt($chs[$key], CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($chs[$key], CURLOPT_RETURNTRANSFER, true);
            curl_setopt($chs[$key], CURLOPT_HTTPHEADER, [
                'Accept: application/json',
                'Content-Type: application/json'
            ]);

            curl_multi_add_handle($mh, $chs[$key]);
        }

        //running the requests
        $running = null;
        do {
            curl_multi_exec($mh, $running);
        } while ($running);

        //getting the responses
        foreach(array_keys($chs) as $key){

            $error = curl_error($chs[$key]);
            $last_effective_URL = curl_getinfo($chs[$key], CURLINFO_EFFECTIVE_URL);
            $res = curl_multi_getcontent($chs[$key]);  // get results

            if (empty($error)) {
                if ($last_effective_URL == Yii::$app->params['articleUrl']) {
                    $articleResponse = json_decode($res,true);
                    $articleResponse = $articleResponse["hits"][array_rand($articleResponse["hits"],1)];

                    $response["title"] = $articleResponse["title"];
                    $response["url"] = $articleResponse["url"];
                    $response["author"] = $articleResponse["author"];

                } else {
                    $imageResponse = json_decode($res,true);
                    $imageResponse['urls']['full'];
                    $response["imageUrl"] = $imageResponse['urls']['full'];
                }

            }

            curl_multi_remove_handle($mh, $chs[$key]);
        }

        // close current handler
        curl_multi_close($mh);

        return $response;
    }
}